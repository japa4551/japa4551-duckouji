And why would you read me?

Adds your local Atomic Duck and some variants to the game.

More Details on the [Steam Workshop Page](https://steamcommunity.com/sharedfiles/filedetails/?id=3164245764)

Roadmap:
- [ ] Rework the explosion size
- [ ] Look into the "Talking Duck" easter egg
- [ ] Finish the Ideology stuff

Special thanks to all the people that helped me on the RimWorld Discord, mainly those four wonderful persons: aelana, steveo.o, espioidsavant and erdelf.

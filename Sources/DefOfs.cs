using System;
using RimWorld;
using UnityEngine;
using Verse;

namespace Duckouji {
    [DefOf]
	public static class DuckoThingDef
	{
		public static ThingDef Duckouji_Duck;
		public static ThingDef Duckouji_Duckobo;
		static DuckoThingDef()
		{
			DefOfHelper.EnsureInitializedInCtor(typeof(DuckoThingDef));
		}
	}
	
	[DefOf]
	public static class DuckPawnKindDef
	{
		public static PawnKindDef Duckouji_Duck;
		public static PawnKindDef Duckouji_Duckobo;
		static DuckPawnKindDef()
		{
			DefOfHelper.EnsureInitializedInCtor(typeof(DuckPawnKindDef));
		}
	}
}
using System;
using RimWorld;
using UnityEngine;
using Verse;

namespace Duckouji {
    public class IncidentWorker_WildDuckouji : IncidentWorker
    {
		private bool TryFindEntryCell(Map map, out IntVec3 cell)
		{
			return RCellFinder.TryFindRandomPawnEntryCell(out cell, map, CellFinder.EdgeRoadChance_Animal + 0.2f);
		}

		protected override bool CanFireNowSub(IncidentParms parms)
		{
			if (parms != null) {
				Map map = (Map)parms.target;
				IntVec3 cell;

				var acceptableTemperature = map.mapTemperature.SeasonAndOutdoorTemperatureAcceptableFor(
					DuckoThingDef.Duckouji_Duck
				);
				
				return acceptableTemperature && TryFindEntryCell(map, out cell);
			}

			return false;
		}

		protected override bool TryExecuteWorker(IncidentParms parms)
        {
			if (parms != null) {
				Map map = (Map)parms.target;
				if (!TryFindEntryCell(map, out var cell))
				{
					return false;
				}

				PawnKindDef duckouji = DuckPawnKindDef.Duckouji_Duck;
				int value = GenMath.RoundRandom(StorytellerUtility.DefaultThreatPointsNow(map) / duckouji.combatPower);
				int max = Rand.RangeInclusive(3, 6);
				value = Mathf.Clamp(value, 2, max);
				int num = Rand.RangeInclusive(90000, 150000);
				IntVec3 result = IntVec3.Invalid;
				if (!RCellFinder.TryFindRandomCellOutsideColonyNearTheCenterOfTheMap(cell, map, 10f, out result))
				{
					result = IntVec3.Invalid;
				}
				Pawn pawn = null;
				for (int i = 0; i < value; i++)
				{
					IntVec3 loc = CellFinder.RandomClosewalkCellNear(cell, map, 10);
					pawn = PawnGenerator.GeneratePawn(duckouji);
					GenSpawn.Spawn(pawn, loc, map, Rot4.Random);
					pawn.mindState.exitMapAfterTick = Find.TickManager.TicksGame + num;
					if (result.IsValid)
					{
						pawn.mindState.forcedGotoPosition = CellFinder.RandomClosewalkCellNear(result, map, 10);
					}
				}
				SendStandardLetter(
					"LetterLabelWildDuckouji".Translate(duckouji.label).CapitalizeFirst(), 
					"LetterWildDuckouji".Translate(duckouji.label), 
					LetterDefOf.PositiveEvent, parms, pawn
				);
				
				return true;
			}

			return false;
        }
	}
}

